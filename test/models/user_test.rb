require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  # проверка допустимости
  def setup
    @user = User.new(name:'Example User', email: 'user@example.com',
                      password: 'foobar', password_confirmation: 'foobar')
  end

  test 'should be valid' do
    assert @user.valid?
  end

# проверка наличия имени
  test 'name should be present' do
    @user.name = ' '
    assert_not @user.valid?
  end

# проверка наличия имени email
  test 'email should be present' do
    @user.email = ' '
    assert_not @user.valid?
  end

  # проверка длины имени польз-ля (не более 51 символа)
  test 'name not should be too long' do
    @user.name = 'a' * 51
    assert_not @user.valid?
  end

  # проверка длины email (не более 244 символа)
  test 'email not should be too long' do
    @user.email = 'a' * 244 + '@example.com'
    assert_not @user.valid?
  end

  # тест для минимальной длины пароля
  test 'password should have a minimum length' do
    @user.password = @user.password_confirmation = 'a' * 5
    assert_not @user.valid?
  end

  # тест для допустимых адресов email
  test 'email validation should accept valid addresses' do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  # тесты для проверки формата email
  test 'email validation should reject invalid addresses }' do
    invalid_addresses = %w[user@example, com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar_baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  # проверка уникальности email
  test 'email addresses should be unique' do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  # проверка преобразования адреса электронной почты в нижний регистр
  test 'email addresses should be saved as lower-case' do
    mixed_case_email = 'Foo@ExAMPle.coM'
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
end
