class UsersController < ApplicationController
  def show
    # определяем переменную экземпляра @user.
    # для извлечения польз-ля из бд исп-ся м-д find модели User
    # для получ-я id польз-ля исп-ся переменная params
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    # в вызов User.new передаем хэш атрибутов пользователя
    @user = User.new(user_params)
    if @user.save
      flash[:success] = 'Welcome to the sample app.'
      redirect_to @user
    else
      render 'new'
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
